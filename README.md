# ArcGISPlottingStarterPack
A Starter Pack for anyone starting out on ArcGIS in Unity that I build during my spare time.

The pack includes:
1. MapManager (For Getting Coordinates based using RaycastHit)
2. MarkerManager (To Create or Delete Marker)
3. Plotting System (To Plot the marker based on a certain info or location)
4. Metadata (A kind of list system that I created to easy myself in searching for the children on GameObjects)

* Note:
- You need your own API from ArcGIS to use this pack
- I understand that there is some error in this code such as the marker wont instantiated exactly where the mousePosition is at, etc. I am still searching on where the problem lies because my project from before does not have any error using this pack. But hey, it's free so try solving the problem yourself.