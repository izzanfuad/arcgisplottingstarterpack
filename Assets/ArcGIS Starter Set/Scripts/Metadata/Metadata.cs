using System.Collections.Generic;

using UnityEngine;

using Meta.Data.Params;

namespace Meta.Data
{
    /**
    * <summary>
    * A script to make it easier to get the child of the object
    * <br> Created By : Izzan A. Fu'ad. </br>
    * </summary>
    */
    public class Metadata : MonoBehaviour
    {
        [SerializeField] List<MetaParam> _params;

        /**
        * <summary>
        * Add a New Parameter
        * </summary>
        * <param name="id"> The ID of the Parameter </param>
        * <param name="parameter"> Parameter tha you want to add </param>
        */
        public void AddParam(string id, GameObject parameter)
        {
            MetaParam newParam = new()
            {
                id = id,
                parameter = parameter
            };

            AddParam(newParam);
        }

        /**
        * <summary>
        * Add a New Parameter
        * </summary>
        * <param name="parameter"> Parameter tha you want to add </param>
        */
        public void AddParam(MetaParam parameter)
        {
            _params.Add(parameter);
        }

        /**
        * <summary>
        * Find the Parameter that You Want
        * </summary>
        * <returns>MetaParam</returns>
        * <param name="parameter">The Parameter that You Want to Find</param>
        */
        public MetaParam FindParam(string parameter)
        {
            MetaParam param = _params.Find(x => x.id == parameter);
            if (param == null)
                return null;

            return param;
        }

        /**
        * <summary>
        * Finding the Component of the Parameter that You Want
        * </summary>
        * <returns>Component</returns>
        * <param name="parameter">The Parameter that You Want to Find</param>
        */
        public Component FindParamComponent<Component>(string parameter)
        {
            return FindParam(parameter).parameter.GetComponent<Component>();
        }

        /**
        * <summary>
        * Show the GameObject.
        * </summary>
        */
        public void Show()
        {
            gameObject.SetActive(true);
        }

        /**
        * <summary>
        * Hide The GameObject.
        * </summary>
        */
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        /**
        * <summary>
        * Remove the Parameter from List
        * </summary>
        * <param name="parameter">The Parameter that You Want to Remove</param>
        */
        public void RemoveParam(string parameter)
        {
            var param = FindParam(parameter);
            if (param == null) return;

            _params.Remove(param);
        }

        /**
        * <summary>
        * Delete the Parameter from List
        * </summary>
        * <param name="parameter">The Parameter that You Want to Delete</param>
        */
        public void DeleteParam(string parameter)
        {
            var param = FindParam(parameter);
            if (param == null) return;

            _params.Remove(param);
            Destroy(param.parameter);
        }

        /**
        * <summary>
        * Show the Parameter
        * </summary>
        * <param name="parameter">Parameter that You Want to be Shown</param>
        */
        public void ShowParam(string parameter)
        {
            var param = FindParam(parameter);
            if (param == null) return;

            param.parameter.SetActive(true);
        }

        /**
        * <summary>
        * Hide the Parameter
        * </summary>
        * <param name="parameter">Parameter that You Want to Hide</param>
        */
        public void HideParam(string parameter)
        {
            var param = FindParam(parameter);
            if (param == null) return;

            param.parameter.SetActive(false);
        }
    }
}