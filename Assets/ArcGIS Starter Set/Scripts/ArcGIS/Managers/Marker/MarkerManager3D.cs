using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;

public class MarkerManager3D : BaseMarkerManager<MarkerManager3D, ArcGISMarker3D>
{
    private Transform container;
    public Transform Container
    {
        get
        {
            if (container == null)
            {
                GameObject _container = new("3D Markers");
                container = _container.transform;
                container.parent = GameObject.Find("ArcGISMap").transform;
                container.localPosition = Vector3.zero;
                container.localRotation = Quaternion.identity;
                container.localScale = Vector3.one;
            }

            return container;
        }
    }

    [SerializeField] private GameObject defaultPrefab;

    private void Start()
    {
        defaultPrefab ??= Resources.Load<GameObject>("Default/Default 3D Marker");
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="position">The Real World Position</param>
    * <param name="rotation">The Real World Rotation</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(GameObject prefab, ArcGISPoint position, ArcGISRotation rotation, string name = "")
    {
        ArcGISMarker3D marker = CreateItem(prefab, position, rotation);
        marker.id = items.Count;
        marker.markerName = name == "" ? prefab.name : name;
        marker.prefab = prefab;
        marker.transform.parent = Container;
        marker.gameObject.name = marker.markerName;

        return marker;
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="position">The Real World Position</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(ArcGISPoint position, ArcGISRotation rotation, string name = "")
    {
        return Create(defaultPrefab, position, rotation, name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="position">The Real World Position</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(GameObject prefab, ArcGISPoint position, string name = "")
    {
        return Create(prefab, position, new(), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="position">The Real World Position</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(ArcGISPoint position, string name = "")
    {
        return Create(defaultPrefab, position, new(), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(GameObject prefab, double longitude, double latitude, string name = "")
    {
        return Create(prefab, new(longitude, latitude, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(double longitude, double latitude, string name = "")
    {
        return Create(defaultPrefab, new(longitude, latitude, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="altitude">The Altitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(GameObject prefab, double longitude, double latitude, double altitude, string name = "")
    {
        return Create(prefab, new(longitude, latitude, altitude, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="altitude">The Altitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public ArcGISMarker3D Create(double longitude, double latitude, double altitude, string name = "")
    {
        return Create(defaultPrefab, new(longitude, latitude, altitude, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Find Instantiated Marker
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="marker">Marker to Find</param>
    */
    public ArcGISMarker3D Find(ArcGISMarker3D marker)
    {
        return FindItem(marker);
    }

    /**
    * <summary>
    * Find Instantiated Marker by ID
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="id">Id of the marker</param>
    */
    public ArcGISMarker3D Find(int id)
    {
        return items.Find(x => x.id == id);
    }

    /**
    * <summary>
    * Destroy Instantiated Marker
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="item">Marker to Destroy</param>
    */
    public void Remove(ArcGISMarker3D marker)
    {
        RemoveItem(marker);
    }

    /**
    * <summary>
    * Destroy Instantiated Marker by ID
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="id">Id of the marker</param>
    */
    public void Remove(int id)
    {
        ArcGISMarker3D marker = Find(id);
        if (marker == null)
            return;

        RemoveItem(marker);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="position">The Real World Position</param>
    * <param name="rotation">The Real World Rotation</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(GameObject prefab, ArcGISPoint position, ArcGISRotation rotation, string name = "")
    {
        return Instance.Create(prefab, position, rotation, name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="position">The Real World Position</param>
    * <param name="rotation">The Real World Rotation</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(ArcGISPoint position, ArcGISRotation rotation, string name = "")
    {
        return Instance.Create(position, rotation, name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="position">The Real World Position</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(GameObject prefab, ArcGISPoint position, string name = "")
    {
        return CreateMarker(prefab, position, new(), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="position">The Real World Position</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(ArcGISPoint position, string name = "")
    {
        return CreateMarker(position, new(), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(GameObject prefab, double longitude, double latitude, string name = "")
    {
        return CreateMarker(prefab, new(longitude, latitude, 0, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(double longitude, double latitude, string name = "")
    {
        return CreateMarker(new(longitude, latitude, 0, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="altitude">The Altitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(GameObject prefab, double longitude, double latitude, double altitude, string name = "")
    {
        return CreateMarker(prefab, new(longitude, latitude, altitude, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Create a 3D Marker with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="longitude">The Longitude of the Marker</param>
    * <param name="latitude">The Latitude of the Marker</param>
    * <param name="altitude">The Altitude of the Marker</param>
    * <param name="name">The Marker's Name</param>
    */
    public static ArcGISMarker3D CreateMarker(double longitude, double latitude, double altitude, string name = "")
    {
        return CreateMarker(new(longitude, latitude, altitude, new ArcGISSpatialReference(4326)), name);
    }

    /**
    * <summary>
    * Find Instantiated Marker
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="marker">Marker to Find</param>
    */
    public static ArcGISMarker3D FindMarker(ArcGISMarker3D marker)
    {
        return Instance.FindItem(marker);
    }

    /**
    * <summary>
    * Find Instantiated Marker by ID
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="id">Id of the marker</param>
    */
    public static ArcGISMarker3D FindMarker(int id)
    {
        return Instance.items.Find(x => x.id == id);
    }

    /**
    * <summary>
    * Destroy Instantiated Marker
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="item">Marker to Destroy</param>
    */
    public static void RemoveMarker(ArcGISMarker3D marker)
    {
        Instance.RemoveItem(marker);
    }

    /**
    * <summary>
    * Destroy Instantiated Marker by ID
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="id">Id of the marker</param>
    */
    public static void RemoveMarker(int id)
    {
        ArcGISMarker3D marker = Instance.Find(id);
        if (marker == null)
            return;

        Instance.RemoveItem(marker);
    }
}
