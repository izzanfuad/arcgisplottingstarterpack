using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;

public class BaseMarkerManager<T, U> : MonoBehaviour
where T : BaseMarkerManager<T, U>
where U : ArcGISLocationComponent
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject map = GameObject.Find("ArcGISMap");
                instance = map.GetComponent<T>() ?? map.AddComponent<T>();
            }
            return instance;
        }
    }

    [HideInInspector] public List<U> items = new();

    /**
    * <summary>
    * Instantiate a GameObject with Real World Position on the ArcGIS Map
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="prefab">The GameObject that will be Instantiated using Real World Position</param>
    * <param name="position">The Real World Position</param>
    * <param name="rotation">The Real World Rotation</param>
    * <param name="name">The Marker's Name</param>
    */
    protected U CreateItem(GameObject prefab, ArcGISPoint position, ArcGISRotation rotation)
    {
        U item = Instantiate(prefab, Instance.transform).AddComponent<U>();
        item.Position = position;
        item.Rotation = rotation;
        items.Add(item);
        return item;
    }

    /**
    * <summary>
    * Find Instantiated Item
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="item">Item to Find</param>
    */
    protected U FindItem(U item)
    {
        return items.Find(x => x == item);
    }

    /**
    * <summary>
    * Destroy Instantiated Item
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <param name="item">Item to Destroy</param>
    */
    protected void RemoveItem(U item)
    {
        items.Remove(item);
        Destroy(item.gameObject);
    }
}
