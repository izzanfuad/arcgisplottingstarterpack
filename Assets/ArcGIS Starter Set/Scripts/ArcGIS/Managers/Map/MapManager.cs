using Unity.Mathematics;
using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;

public class MapManager : MonoBehaviour
{
    public static MapManager instance;

    private ArcGISMapComponent map;
    private ArcGISPoint currentPoint;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            map = GetComponent<ArcGISMapComponent>();
        }
        else
            Destroy(this);
    }

    private void LateUpdate()
    {
        RaycastCheck();
    }

    /**
    * <summary>
    * Checks if the Current Mouse Position is on the Map or Not.
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    */
    private void RaycastCheck()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mousePos);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            ArcGISPoint geoPos = map.EngineToGeographic(hit.point);
            currentPoint = GeoUtils.ProjectToSpatialReference(geoPos, new ArcGISSpatialReference(4326));
            return;
        }

        currentPoint = null;
    }

    /**
    * <summary>
    * Grabs the Current Coords if the Mouse is on the Map.
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    */
    private ArcGISPoint GetCoords()
    {
        return currentPoint;
    }

    /**
    * <summary>
    * Checks if the Current Mouse Position is on the Map or Not.
    * <br>Gives the result using ArcGISPoint.</br>
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <returns>
    * True if on Map.
    * <br>False if not</br>
    * </returns>
    */
    public static bool GetCoords(out ArcGISPoint point)
    {
        point = null;
        if (instance == null)
            return false;

        point = instance.GetCoords();
        return point != null;
    }

    /**
    * <summary>
    * Checks if the Current Mouse Position is on the Map or Not.
    * <br>Gives the result using double.</br>
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <returns>
    * True if on Map.
    * <br>False if not</br>
    * </returns>
    */
    public static bool GetCoords(out double longitude, out double latitude, out double altitude)
    {
        if (GetCoords(out ArcGISPoint point))
        {
            longitude = point.X;
            latitude = point.Y;
            altitude = point.Z;
            return true;
        }

        longitude = 0;
        latitude = 0;
        altitude = 0;
        return false;
    }


    /**
    * <summary>
    * Checks if the Current Mouse Position is on the Map or Not.
    * <br>Gives the result using ArcGISPoint (Without altitude).</br>
    * <br>Created by: Izzan A. Fu'ad</br>
    * </summary>
    * <returns>
    * True if on Map.
    * <br>False if not</br>
    * </returns>
    */
    public static bool GetCoords(out double longitude, out double latitude)
    {
        return GetCoords(out longitude, out latitude, out double altitude);
    }
}
