using UnityEngine;

using Esri.ArcGISMapsSDK.Components;

public class ArcGISMarker3D : ArcGISLocationComponent
{
    public int id;
    public string markerName;

    [HideInInspector] public GameObject prefab;
}