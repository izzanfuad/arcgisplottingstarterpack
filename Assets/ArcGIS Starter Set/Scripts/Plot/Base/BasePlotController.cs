using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Esri.ArcGISMapsSDK.Components;

public class BasePlotController<T, U> : MonoBehaviour
where T : BasePlotController<T, U>
where U : BaseObject
{
    public static T instance;
    protected readonly List<U> objects = new();

    private void Awake()
    {
        if (instance == null)
            instance = GetComponent<T>();
        else
            Destroy(this);
    }

    protected void Delete(U obj)
    {
        objects.Remove(obj);

        ArcGISMarker3D marker = obj.GetComponent<ArcGISMarker3D>();
        MarkerManager3D.RemoveMarker(marker);
    }
}
