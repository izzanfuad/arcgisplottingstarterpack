using UnityEngine;

public class BaseEntity
{
    public string id;
    public string name;
    public double longitude;
    public double latitude;
    public double altitude;
}