using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

using Esri.ArcGISMapsSDK.Components;

public class BaseObject : MonoBehaviour
{
    public string objName;
    public BaseEntity entity;
    public ArcGISLocationComponent marker;

    public UnityEvent onClick = new();

    private void OnMouseDown()
    {
        onClick.Invoke();
    }
}
