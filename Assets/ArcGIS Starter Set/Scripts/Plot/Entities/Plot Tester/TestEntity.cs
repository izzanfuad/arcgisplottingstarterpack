using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;

public class TestEntity : BaseEntity
{
    public ArcGISPoint WorldPos => new(longitude, latitude, altitude, new ArcGISSpatialReference(4326)); 
}