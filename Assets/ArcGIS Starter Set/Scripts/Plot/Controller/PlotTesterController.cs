using System.Collections;
using System.Collections.Generic;
using Esri.GameEngine.Geometry;

using Meta.Data;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlotTesterController : BasePlotController<PlotTesterController, TestObject>
{
    [SerializeField] private GameObject canvas;
    [SerializeField] private bool isPlotting;
    [SerializeField] private TestObject currentObj;

    private void Update()
    {
        if (isPlotting)
            Plot();
    }

    public void OpenForm()
    {
        ClearForm();
        canvas.SetActive(true);
    }

    private void EditForm(TestObject obj)
    {
        OpenForm();
        FillForm(obj);
        currentObj = obj;
    }

    public void CloseForm()
    {
        canvas.SetActive(false);
    }

    private void FillForm(TestObject obj)
    {
        Metadata meta = canvas.GetComponent<Metadata>();
        meta.FindParamComponent<TMP_InputField>("inputName").text = obj.entity.name;
        meta.FindParamComponent<Button>("editBtn").onClick.AddListener(delegate { Edit(obj); });
        meta.FindParamComponent<Button>("deleteBtn").onClick.AddListener(delegate { Destroy(obj); });
        meta.HideParam("plotBtn");
        meta.ShowParam("editGroup");
    }

    private void ClearForm()
    {
        Metadata meta = canvas.GetComponent<Metadata>();
        meta.FindParamComponent<TMP_InputField>("inputName").text = string.Empty;
        meta.FindParamComponent<Button>("editBtn").onClick.RemoveAllListeners();
        meta.FindParamComponent<Button>("deleteBtn").onClick.RemoveAllListeners();
        meta.ShowParam("plotBtn");
        meta.HideParam("editGroup");
        currentObj = null;
    }

    public void SubmitForm()
    {
        if (CheckForm(out string error))
        {
            Debug.LogWarning(error);
            return;
        }

        canvas.SetActive(false);
        isPlotting = true;
    }

    private bool CheckForm(out string error)
    {
        Metadata meta = canvas.GetComponent<Metadata>();
        TMP_InputField inputName = meta.FindParamComponent<TMP_InputField>("inputName");
        if (inputName.text == string.Empty)
        {
            error = "You Have Not Fill the Name Field";
            return true;
        }

        error = string.Empty;
        return false;
    }

    private void Plot()
    {
        Metadata meta = canvas.GetComponent<Metadata>();
        if (Input.GetMouseButtonDown(0))
            if (MapManager.GetCoords(out double longitude, out double latitude, out double altitude))
            {
                TestEntity entity = new()
                {
                    id = CreateUniqueName(),
                    name = meta.FindParamComponent<TMP_InputField>("inputName").text,
                    longitude = longitude,
                    latitude = latitude,
                    altitude = altitude
                };

                Create(entity);
                isPlotting = false;
            }
    }

    private void Create(TestEntity entity)
    {
        ArcGISMarker3D marker = MarkerManager3D.CreateMarker(entity.WorldPos, entity.id);
        TestObject testObject = marker.gameObject.AddComponent<TestObject>();
        testObject.objName    = marker.name;
        testObject.entity     = entity;
        testObject.marker     = marker;
        testObject.onClick.AddListener(delegate { EditForm(testObject); });
    }

    private void Edit(TestObject obj)
    {
        if (CheckForm(out string error))
        {
            Debug.LogWarning(error);
            return;
        }

        Metadata meta = canvas.GetComponent<Metadata>();
        obj.entity.name = meta.FindParamComponent<TMP_InputField>("inputName").text;
        canvas.SetActive(false);
    }

    private void Destroy(TestObject obj)
    {
        Delete(obj);
        canvas.SetActive(false);
    }

    private string CreateUniqueName()
    {
        return $"testObject_{objects.Count}_{Random.Range(1, 1000)}";
    }
}
